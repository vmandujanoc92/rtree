#ifndef RTREE_H
#define RTREE_H

#include <vector>
#include "MBR.h"

#include <cstdlib>
#include <iostream>
using namespace std;

class RTree 
{
private:
    MBR* head;
    int M,m; //para saber cuantos hijos debe tener cada nodo   
public:
	RTree(int ord=3);
	void insert(vector<pair<int,int>> polygon);//inserta polygon en el rtree
	bool getmbr(vector<vector<pair<int,int>>>& mbrs);//devuelve todos los MBRs del rtree
	bool range(vector<pair<int, int>> polygon, vector<vector<pair<int, int>>>& objects);//retorna los objects dentro de polygon
	bool nearest(int k, vector<pair<int, int>> point, vector<vector<pair<int, int>>>& objects);//retorn los k objects m�s cercanos a point
	bool split();
	bool deleteAll();//Elimina todos los objetos del rtree
};

RTree::RTree(int ord=2)
{
    M=ord;
    m=(ord+1)/2;
    head=new MBR;
    head->coMbr=NULL;
    head->subMbr=NULL;        
}

RTree::insert(vector<pair<int,int>> polygon)
{
    MBR* fig = new MBR(polygon);
    
    if(head->subMbr==NULL && head->coMbr==NULL){//la primerafigura insertada
         head->subMbr=fig;
         head->setXY(fig);//Modifica el MBR's del recorrido            
    }else{
         Node* temp=head;
         Node* temp2;
         int area=0;
         //vector<MBR*> v; v.push_back(temp); se a�ade a una pila
         while(temp->estado){//busca el MBR que se ajuste a fig. y asignarlo
              temp2=temp;
              area=temp->areaBetween(fig);
              while(temp->coMbr!=NULL){
                     temp=temp->coMbr;
                     if((temp->areaBetween(fig))<area){
                          area=temp->areaBetween(fig);
                          temp2=temp;
                     }
              }
              temp=temp2;
              //v.push_back(temp); se a�ade a una pila
              temp=temp->subMbr;//revisar,puede haber un error                        
         }
         temp=temp2;//insertar
         temp2=temp->subMbr;
         temp->subMbr=fig;
         fig->coMbr=temp2;
         //===========================================
         //todavia usamos temp
         if(M<(temp->count())){//verificar la cantidad para hacer el split
              split();//el split
              temp2=head;// revisar!!!!                    
         }
         
         
         //Modificar MBR's del recorrido                             
    }                       
}

bool RTree::deleteAll()
{
     
}
#endif
