//============================================================================
// Name        : RTree.cpp
// Author      : Daniel Palomino
// Version     : 0.1
// Copyright   : Your copyright notice
// Description : RTree Backend
// Created on  : 08 ago. 2018
//============================================================================

#include "RTree.h"

RTree::RTree() {
	return;
}

bool RTree::split() {
	return true;
}

bool RTree::insert(vector<pair<int, int>> polygon) {
	return true;
}

bool RTree::mbr(vector<vector<pair<int, int>>>& mbrs) {
	return true;
}

bool RTree::range(vector<pair<int, int>> polygon, vector<vector<pair<int, int>>>& objects) {
	return true;
}

bool RTree::nearest(int k, vector<pair<int, int>> point, vector<vector<pair<int, int>>>& objects) {
	return true;
}

bool RTree::deleteAll() {
	return true;
}